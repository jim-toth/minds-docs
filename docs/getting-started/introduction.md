---
id: introduction
title: Welcome to the Minds stack
sidebar_label: Introduction
---

Minds is a free & open-source, encrypted and reward-based social networking platform. Our [roadmap](https://gitlab.com/groups/minds/-/roadmap), [code](https://gitlab.com/minds/minds), [project management system](https://gitlab.com/groups/minds/-/boards), and more all reside in [GitLab](https://gitlab.com/minds).

## Repositories

The [Minds repository](https://gitlab.com/Minds/minds) contains multiple git submodule repositories:

- [Engine](https://gitlab.com/Minds/engine) - Backend code & APIs
- [Front](https://gitlab.com/Minds/front) - Client side Angular2 web app
- [Sockets](https://gitlab.com/Minds/sockets) - WebSocket server for real-time communication
- [Mobile](https://gitlab.com/Minds/mobile-native) - React Native mobile apps

## Security reports

Please report all security issues to [security@minds.com](mailto:security@minds.com).

## License

[AGPLv3](https://www.minds.org/docs/license.html). Please see the license file of each repository.

**_Copyright Minds 2012 - 2019_**

Copyright for portions of Minds are held by [Elgg](http://elgg.org), 2013 as part of the [Elgg](http://elgg.org) project. All other copyright for Minds is held by Minds, Inc.
